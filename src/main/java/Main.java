import javax.bluetooth.*;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import java.io.*;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.Vector;

public class Main {

    public static final Vector devicesDiscovered = new Vector();
    public static final Vector serviceFound = new Vector();

    public static void main(String[] args) throws IOException, InterruptedException {

        final Object inquiryCompletedEvent = new Object();
        final Object serviceSearchCompletedEvent = new Object();

        devicesDiscovered.clear();
        serviceFound.clear();

        /*UUID serviceUUID = OBEX_OBJECT_PUSH;
        if ((args != null) && (args.length > 0)) {
            serviceUUID = new UUID(args[0], false);
        }*/

        DiscoveryListener listener = new DiscoveryListener() {

            public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
                System.out.println("Device " + btDevice.getBluetoothAddress() + " found");
                devicesDiscovered.addElement(btDevice);
                try {
                    System.out.println(devicesDiscovered.size() + ") name " + btDevice.getFriendlyName(false));
                } catch (IOException cantGetDeviceName) {
                }
            }

            public void inquiryCompleted(int discType) {
                System.out.println("Device Inquiry completed!");
                synchronized(inquiryCompletedEvent){
                    inquiryCompletedEvent.notifyAll();

                    System.out.println("Device : ");
                    Scanner scanner = new Scanner(System.in);
                    int nb = scanner.nextInt();

                    String url = "btspp://localhost:" + ((RemoteDevice) devicesDiscovered.get(nb - 1)).getBluetoothAddress() + ";name=BlueToothServer";
                    //String url = "btspp://localhost:C0EEFBD86AC0;name=BlueToothServer";
                    try {
                        StreamConnectionNotifier server = (StreamConnectionNotifier) Connector.open(url);
                        StreamConnection connection = server.acceptAndOpen();
                        InputStream inputStream = connection.openInputStream();
                        InputStreamReader ins = new InputStreamReader(inputStream);
                        OutputStream outputStream = connection.openOutputStream();
                        OutputStreamWriter ous = new OutputStreamWriter(outputStream);

                        while (true) {
                            int val = ins.read();
                            ous.write(25);
                            ous.flush();
                            System.out.println(">> " + (char) val);
                            if (((char) val) == 'x')
                                break;
                        }

                        connection.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            public void serviceSearchCompleted(int transID, int respCode) {
                System.out.println("service search completed!");
                synchronized(serviceSearchCompletedEvent){
                    serviceSearchCompletedEvent.notifyAll();
                }
            }

            public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
                for (int i = 0; i < servRecord.length; i++) {
                    String url = servRecord[i].getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
                    if (url == null) {
                        continue;
                    }
                    serviceFound.add(url);
                    DataElement serviceName = servRecord[i].getAttributeValue(0x0100);
                    if (serviceName != null) {
                        System.out.println("service " + serviceName.getValue() + " found " + url);
                    } else {
                        System.out.println("service found " + url);
                    }
                }
            }
        };

        synchronized(inquiryCompletedEvent) {
            boolean started = LocalDevice.getLocalDevice().getDiscoveryAgent().startInquiry(DiscoveryAgent.GIAC, listener);
            if (started) {
                System.out.println("wait for device inquiry to complete...");
                inquiryCompletedEvent.wait();
                System.out.println(devicesDiscovered.size() +  " device(s) found");
            }
        }

        /*UUID[] searchUuidSet = new UUID[] { serviceUUID };
        int[] attrIDs =  new int[] {
                0x0100 // Service name
        };*/

        /*for(Enumeration en = RemoteDeviceDiscovery.devicesDiscovered.elements(); en.hasMoreElements(); ) {
            RemoteDevice btDevice = (RemoteDevice)en.nextElement();

            synchronized(serviceSearchCompletedEvent) {
                System.out.println("search services on " + btDevice.getBluetoothAddress() + " " + btDevice.getFriendlyName(false));
                LocalDevice.getLocalDevice().getDiscoveryAgent().searchServices(attrIDs, searchUuidSet, btDevice, listener);
                serviceSearchCompletedEvent.wait();
            }
        }*/
    }
}
